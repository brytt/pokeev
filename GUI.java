/**
 * @(#)GUI.java
 *
 *
 * @author
 * @version 1.00 2010/3/29
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class GUI {
	JFrame frame;
	JPanel panel1;
	JPanel panel2;
	
	JTextField name;
	JLabel[] statNames;
	JSpinner[] stats;
	JButton[][] buttons;

    public GUI() {
    	frame = new JFrame("Flib's EV Tracker");
    	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	panel1 = new JPanel();
    	panel2 = new JPanel();
    	
    	name = new JTextField("Name");
    	statNames = new JLabel[6];
    	stats = new JSpinner[6];
    	buttons = new JButton[4][6];

    	panel1.setLayout(new GridLayout(2,1));
    	panel2.setLayout(new GridLayout(6,6));
    	
    	frame.add(panel1);
    	panel1.add(name);
    	panel1.add(panel2);
    	
    	statNames[0] = new JLabel("HP");
    	statNames[1] = new JLabel("Atk");
    	statNames[2] = new JLabel("Def");
    	statNames[3] = new JLabel("SpA");
    	statNames[4] = new JLabel("SpD");
    	statNames[5] = new JLabel("Spe");
    	
    	ButtonListener listen = new ButtonListener();
    	
    	for(int i=0;i<6;i++)	{
    		stats[i] = new JSpinner();
    		buttons[0][i] = new JButton("+1");
    		buttons[1][i] = new JButton("+2");
    		buttons[2][i] = new JButton("+4");
    		buttons[3][i] = new JButton("+5");
    		
    		panel2.add(statNames[i]);
    		panel2.add(stats[i]);
    		for(int j=0;j<4;j++)	{
    			buttons[j][i].addActionListener(listen);
    			panel2.add(buttons[j][i]);
    		}
    	}
    	frame.pack();
    	frame.setVisible(true);
    }

	private class ButtonListener implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			int i = 0;
			int j = 0;
			boolean found = false;
			JButton clicked = (JButton)e.getSource();
			
			//System.out.println("===========================");
			/*for(i=0;i<4&&!found;i++)
				for(j=0;j<6&&!found;j++)	{
					found = buttons[i][j] == clicked;
					System.out.println("i: "+i+"\nj: "+j+"\nfound: "+found);
				}*/
			while(i < 4 && !found)	{
				j = 0;
				while(j < 6 && !found)	{
					if(buttons[i][j] == clicked)
						found = true;
					else
						j++;
					
					//System.out.println("i: "+i+"\nj: "+j+"\nfound: "+found);
				}
				if(!found)
					i++;
			}
			//System.out.println(i+", "+j);
			
			if(i < 4 && j < 6)	{
				int amount = 0;
				switch(i)	{
					case 0:amount = 1;
					break;
					case 1:amount = 2;
					break;
					case 2:amount = 4;
					break;
					case 3:amount = 5;
					break;
				}
				/*if(i == 0)
					amount = 1;
				else if(i == 1)
					amount = 2;
				else if(i == 2)
					amount = 4;
				else
					amount = 5;*/
					
				stats[j].setValue((Integer)stats[j].getValue() + amount);
			}
		}
	}
}